About
=====

This work is part of the mOSAIC project open-source outcomes released under the Apache 2.0 license (see the "Notice" section below).
    http://developers.mosaic-cloud.
    
This module provides with a HDSF implementation of the mOSAIC Distributed FileSystem connect API (mosaic-java-connectors-dfs)


How to build
============

**It should be built** from the ``mosaic-distribution`` repository, as it depends on a proper set workbench and symlinks to other repositories.


Notice
======
This product includes software developed at "XLAB d.o.o.", as part of the
"mOSAIC -- Open-Source API and Platform for Multiple Clouds"
research project (an EC FP7-ICT Grant, agreement 256910).

    http://developers.mosaic-cloud.eu/
    http://www.xlab.si/

Developers:
 * Mariano Cecowski: mariano.cecowski(at)xlab.si / marianocecowski(at)gmail.com
 * Boris Savic: boris.savic(at)xlab.si

Copyright: ::

Copyright 2010-2013, XLAB d.o.o., Ljubljana, Slovenia
    http://www.xlab.si/

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at:
    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
