/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import junit.framework.Assert;

import org.apache.hadoop.fs.FileSystem;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;

import eu.mosaic_cloud.drivers.filesystem.hdfs.HDFSDriver;
import eu.mosaic_cloud.platform.core.configuration.IConfiguration;
import eu.mosaic_cloud.platform.core.configuration.PropertyTypeConfiguration;
import eu.mosaic_cloud.tools.exceptions.tools.NullExceptionTracer;
import eu.mosaic_cloud.tools.exceptions.tools.QueueingExceptionTracer;
import eu.mosaic_cloud.tools.threading.implementations.basic.BasicThreadingContext;
import eu.mosaic_cloud.tools.threading.implementations.basic.BasicThreadingSecurityManager;
import eu.mosaic_cloud.tools.transcript.core.Transcript;
import eu.mosaic_cloud.tools.transcript.tools.TranscriptExceptionTracer;

public class HDFSDriverTest {

	static IConfiguration config;
	static FileSystem hdfs;
	static BasicThreadingContext threadingContext;
	HDFSDriver driver;

    static final String fileA = "/test/largeFile.txt";
    static final String fileB = "/test/copiedFile.txt";
    static final String fileC = "/test/movedFile.txt";
    static final String fileD = "/test/testFile1.txt";
    static final String fileE = "/test/writeRead100byte.txt";
    static final String dirA = "/newDirA";
    static final String dirB = "/newDirB";
    static final String testDir  = "/test";
    static final String message = "writing new content to file";
    static final String content = "THE YEAR 1866 was marked by a bizarre development, an unexplained and downright inexplicable phenome";
    
	static Logger logger;
    @BeforeClass
    public static void setUpBeforeClass() {
    	config = PropertyTypeConfiguration.create();
    	config.addParameter("hdfs.host", "localhost");
    	config.addParameter("hdfs.port", 9000);
    	config.addParameter("hdfs.threads", 1);
	}
	 
    @Before
    public void setUp() throws IOException {
    	
        final Transcript transcript = Transcript.create(this);
        final QueueingExceptionTracer exceptionsQueue = QueueingExceptionTracer
                .create(NullExceptionTracer.defaultInstance);
        final TranscriptExceptionTracer exceptions = TranscriptExceptionTracer.create(transcript,
                exceptionsQueue);
        BasicThreadingSecurityManager.initialize();
        threadingContext = BasicThreadingContext.create(this, exceptions, exceptions.catcher);
        threadingContext.initialize();
		driver = HDFSDriver.create(config, threadingContext);
		logger = Transcript.create (this, true).adaptAs (Logger.class);
    }
    
    @After
    public void tearDown() {
    	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
        this.driver.destroy();
        threadingContext.destroy();
    }
    
    @Test
    public void testDirectoryOperations()
    {
    	try {
			testMkDir();
	    	testMoveDir();
	    	testRmDir();
		} catch (ExecutionException e) {
			logger.error("ExecutionException running HDFSDriver tests", e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			logger.error("InterruptedException running HDFSDriver tests", e.getMessage());
			e.printStackTrace();
		}
    }

    
    @Test
    public void testFilehandlerOperation(){
    	try{
    		testFileHandlerWrite();
    		testFileHandlerRead();
    		testFileHandlerReadN();
    		testFileHandlerWrite100byte();
    		testFileHandlerReadN100byte();
		} catch (ExecutionException e) {
			logger.error("ExecutionException running HDFSDriver tests", e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			logger.error("InterruptedException running HDFSDriver tests", e.getMessage());
			e.printStackTrace();
		}
    }
    
    @Test
    public void testDirectoryListing(){
    	try{
    		testList();
		} catch (ExecutionException e) {
			logger.error("ExecutionException running HDFSDriver tests", e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			logger.error("InterruptedException running HDFSDriver tests", e.getMessage());
			e.printStackTrace();
		}
    }
    
    @Test
    public void testFileOperations(){
    	try {
	    	testCopy();
	    	testMove();
	    	testCantRemoveFile();
	    	testRemoveFile();
		} catch (ExecutionException e) {
			logger.error("ExecutionException running HDFSDriver tests", e.getMessage());
			e.printStackTrace();
		} catch (InterruptedException e) {
			logger.error("InterruptedException running HDFSDriver tests", e.getMessage());
			e.printStackTrace();
		}
    	
    }
    
    
//    @Test
    public void testAll() throws InterruptedException, ExecutionException {
//    	testRemoveFile();
//    	testMove();
//    	testCopy();
//    	testList();
//    	testFileHandlerWrite();
//    	testFileHandlerRead();
//    	testFileHandlerClose();

    	System.out.print("Finalizing");
    }
	

    /*
     * File handle operations
     */

    
    public void testFileHandlerReadN() throws InterruptedException, ExecutionException
    {
    	
    	AbstractFileHandlerDriver fileDriver = this.driver.openFile( fileA, FilesystemModes.READ_SEEKABLE);
    	Assert.assertNotNull("File open not successful (read)", fileDriver);
    	
    	byte[] result = fileDriver.read(23);
    	
    	while(result!=null && result.length>0)
    	{
    		System.out.print(new String(result));
    		result = fileDriver.read(23);
    	}

		boolean closed = fileDriver.close();
		Assert.assertTrue("failed close file", closed);
		fileDriver.destroy();
		fileDriver=null;
		
    	
    }
    
	public void testFileHandlerRead() throws InterruptedException, ExecutionException {
	    	
    	AbstractFileHandlerDriver fileDriver = this.driver.openFile( fileA, FilesystemModes.READ_SEEKABLE);
    	Assert.assertNotNull("File open not successful (read)", fileDriver);
    	

    	byte[] result = fileDriver.read(23);
    	Assert.assertNotNull("Nothing was read", result);
    	Assert.assertTrue("Number of read bytes doesn't match!", result.length==23);
   		System.out.print(new String(result));

		boolean closed = fileDriver.close();
		Assert.assertTrue("failed to close file", closed);
		fileDriver.destroy();
		fileDriver=null;

	}

	public void testFileHandlerWrite() throws InterruptedException, ExecutionException {
		
    	AbstractFileHandlerDriver fileDriver = this.driver.openFile(fileA, FilesystemModes.OVERWRITE_SEQUENTIAL);
    	Assert.assertNotNull("File open not successful (write)", fileDriver);

    	boolean result;
    	result = fileDriver.write("Hello World!\nThis is a simple test to try to write some bytes.\n".getBytes());
    	Assert.assertTrue("Data written", result);
    	result = fileDriver.write("These lines should be in the same order as they are here sent to be written.\n".getBytes());
    	Assert.assertTrue("Data written", result);
    	result = fileDriver.write("And there should be no overlapping.\nGood bye World!\n".getBytes());
    	Assert.assertTrue("Data written", result);
    	
    	System.out.println("Closing file...");
	        	
    	Assert.assertTrue("Data written", result);
    	
		result = fileDriver.close();
		Assert.assertTrue("failed close file", result);
		fileDriver.destroy(); //if not called operation never ends...
		fileDriver=null;
	}
    
	
	public void testFileHandlerWrite100byte() throws InterruptedException, ExecutionException 
	{
    	AbstractFileHandlerDriver fileDriver = this.driver.openFile( fileE, FilesystemModes.OVERWRITE_SEQUENTIAL);
    	Assert.assertNotNull("File open not successful (write)", fileDriver);
    	
    	boolean result = fileDriver.write(content.getBytes());
    	Assert.assertTrue("Data written", result);
    	
		result = fileDriver.close();
		Assert.assertTrue("failed lose file tests", result);
		fileDriver.destroy(); //if not called operation never ends...
		fileDriver=null;	
	}
	
    public void testFileHandlerReadN100byte() throws InterruptedException, ExecutionException
    {
    	
    	AbstractFileHandlerDriver fileDriver = this.driver.openFile( fileE, FilesystemModes.READ_SEEKABLE);
    	Assert.assertNotNull("File open not successful (read)", fileDriver);
    	final StringBuffer buff = new StringBuffer();
    	
    	byte[] buffert = fileDriver.read(100);
    	
    	while(buffert!=null && buffert.length>0)
    	{
    		buff.append(new String(buffert));
    		buffert = fileDriver.read(100);
    	}
    	
    	System.out.println("\nEOF - readN\n");
    	
		String readResult = buff.toString();
		boolean equal = readResult.equals(content);
		
		if(equal)
			System.out.println("Read string is equal to written one");
		
		Assert.assertTrue("Read equal to written content", equal);
		
		
		boolean result = fileDriver.close();
		Assert.assertTrue("failed close file", result);
		fileDriver.destroy();
		fileDriver=null;
    }
	
	
	
	public void testFileHandlerClose() throws InterruptedException, ExecutionException 
	{
		AbstractFileHandlerDriver fileDriver = this.driver.openFile( fileA, FilesystemModes.READ_SEQUENTIAL);
		Assert.assertNotNull("File open test not successful", fileDriver);
		boolean result = fileDriver.close();
		Assert.assertTrue("failed close file tests", result);
		fileDriver.destroy();
		fileDriver=null;
	}
	
    public void testList() throws InterruptedException, ExecutionException
    {
    	List<LsElement> r1 = this.driver.listDirectory(testDir);
    	
    	for(LsElement el : r1){
    		System.out.println(el.getName());
    	}
    }
    
    
    /*
     * File system operations
     */
    
	public void testCopy() throws ExecutionException, InterruptedException
	{
		boolean r1 = driver.copyFile(fileA, fileB);
    	
   		Assert.assertTrue("failed file copy tests", r1);
	}
	
	public void testMove() throws InterruptedException, ExecutionException
	{

        boolean r = driver.moveFile(fileB, fileC);
        Assert.assertTrue("File move test not successful", r);
        
	}
	
	public void testCantRemoveFile() throws InterruptedException, ExecutionException
	{
        boolean r =driver.removeFile(fileB);
        Assert.assertFalse("File remove non existent file test not successful", r);
	}
	
	public void testRemoveFile() throws InterruptedException, ExecutionException
	{
        boolean r =driver.removeFile(fileC);
        Assert.assertTrue("File remove test not successful", r);
	}

/*
 * Directory operation test-helper functions
 */
	
    public void testMkDir() throws ExecutionException, InterruptedException
    {
    	boolean r1 = this.driver.makeDirectory(dirA);
    	
   		Assert.assertTrue("Error testing directory creation", r1);
    }
    
    
    public void testMoveDir() throws ExecutionException, InterruptedException
    {
    	boolean r1 = this.driver.moveFile(dirA, dirB);
    	
   		Assert.assertTrue("Error testing directory move", r1);
    }
    
    
    public void testRmDir() throws ExecutionException, InterruptedException
    {
    	boolean result = this.driver.removeDirectory(dirB);
    	Assert.assertTrue("Error testing directory remove", result);
    }
    
    
    /*
     * Other tools
     */
    
	public class Check
	{
		private boolean value=false;
		Check(boolean truth)
		{
			value=truth;
		}
		
		public boolean isChecked()
		{
			return value;
		}
		
		public void setChecked()
		{
			value=true;
		}

		public void setUnchecked()
		{
			value=false;
		}
		
	}
	
}
