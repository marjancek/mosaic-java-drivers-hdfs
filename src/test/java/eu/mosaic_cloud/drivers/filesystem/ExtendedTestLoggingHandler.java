/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem;
import org.slf4j.Logger;

import eu.mosaic_cloud.platform.core.tests.TestLoggingHandler;
import eu.mosaic_cloud.tools.transcript.core.Transcript;


public class ExtendedTestLoggingHandler<T extends Object> extends TestLoggingHandler<T>{

	protected String testName = "";
	protected final Logger logger;
	protected T result=null;
	protected T expectedValue=null;


	public ExtendedTestLoggingHandler(String testName, T expectedValue) {
		super(testName);
		this.testName = testName;
		this.expectedValue = expectedValue;
		final Transcript transcript = Transcript.create (this, true);
		logger = transcript.adaptAs(Logger.class);
	}

	@Override
	public void onFailure(Throwable error){
//		this.logger.trace("ExtendedTestLoggingHandler::" + testName + " failure:" +error.getMessage());
		super.onFailure(error);
	}

	@Override
	public void onSuccess(T result) {

		if(result instanceof byte[])
		{
			byte[] bytes = (byte[]) result;
			byte[] expected =  (byte[]) expectedValue;

			if(expected!=null)
				this.logger.trace("Test " + this.testName + " finished with byte[" + bytes.length + 
						"] of expected " + expected.length);
			else
				this.logger.trace("Test " + this.testName + " finished with byte[" + bytes.length + "]");

		}
		else if (result instanceof String)
		{
			if(expectedValue!=null && expectedValue instanceof String && !result.equals(expectedValue))
				this.logger.trace("Test " + this.testName + " finished with result: " + result +
						"but the expected value was " + expectedValue);
			else
				this.logger.trace("Test " + this.testName + " finished with result: " + result);
		}
		else if (result instanceof Boolean)
		{
			if(expectedValue!=null && expectedValue instanceof Boolean && result!=expectedValue)
				this.logger.trace("Test " + this.testName + " finished with result: " + result +
						", but the expected value was " + expectedValue);
			else
				this.logger.trace("Test " + this.testName + " finished with result: " + result);
		}

		this.result = result;
	}

	public T getResult()
	{
		return result;
	}

}

