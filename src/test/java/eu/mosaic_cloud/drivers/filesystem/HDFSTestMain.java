/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;

public class HDFSTestMain {

	static Configuration config = new Configuration();//false);
	static FileSystem hdfs;
	
//	static final Path filePath = new Path("hdfs://194.102.62.135:9000/user/Boris/testfile.txt");
//	static final Path filePath = new Path("hdfs://194.102.62.135:9000/user/tmp/testfile.txt");
//	static final Path filePath = new Path("hdfs://194.102.62.135:9000/test/testFile1.txt");
	static final Path filePath = new Path("/test/largeFile.txt");
	
	public HDFSTestMain() {
	}

	public static void testWrite() throws FileNotFoundException
	{
		
		  String content = "Hello There!";
		
		  FileInputStream fstream = new FileInputStream("/home/boris/mOSAIC/large.txt");
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  
//		  byte[] buff = content.getBytes();
		  
		  try{		  
			  
			  FSDataOutputStream outputStream = hdfs.create(filePath, true);
			
			  String strLine;
			  while ((strLine = br.readLine()) != null)   {
				  byte[] buffer = strLine.getBytes();
				  outputStream.write(buffer, 0, buffer.length);
				  
			  }
			  outputStream.close();
		  }catch (Exception e){
			  System.out.println(e.getMessage());
		  }
		
	}

	public static void testLs()
	{
		  
		  try{		  
			  File dir = new File("hdfs://localhost:9000/test/");

			  
			  String[] list = FileUtil.list(dir);
			  for(String s : list)
			  {
				  System.out.println("Found: "+s);
			  }
			  
		  }catch (Exception e){
			  System.out.println(e.getMessage());
		  }
		
	}

	public static Boolean testDelete()
	{
		Boolean success = false;
		try{
		  File dir = new File(hdfs.getWorkingDirectory().toString());
		  
		  success = hdfs.delete(filePath, true);
		  
		}catch (Exception e){
			System.out.println(e.getMessage());
	  	}
		return success;
	}
	
	public static Boolean testCopy()
	{
		Boolean success = false;
		try{
		  File dir = new File(hdfs.getWorkingDirectory().toString());
//		  Path filePath2 = new Path("hdfs://194.102.62.135:9000/user/Boris/testfile2.txt");
		  Path filePath2 = new Path("hdfs://localhost:9000/newDirA/testfile2.txt");

		  success = FileUtil.copy(	
					hdfs, filePath, 
					hdfs, filePath2,
					false, config
				);
		  
		}catch (Exception e){
			System.out.println(e.getMessage());
	  	}
		return success;
	}
	
	public static void testMkdir()
	{
		  
		  try{		  
//			  File dir = new File("hdfs://194.102.62.135:9000/user/mariano/");
//			  File dir = new File(hdfs.getWorkingDirectory().toString());
			  
			  Boolean success = hdfs.mkdirs(new Path("/newDirA"));
			  
		  }catch (Exception e){
			  System.out.println(e.getMessage());
		  }
		
	}
	
	public static void testRmdir(){
		  File dir = new File(hdfs.getWorkingDirectory().toString());
		  
		  try {
			Boolean success = hdfs.delete(new Path("/newDirA"), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
	}

	public static void testListStatus()
	{
		  
		  try{		  
//			  File dir = new File("hdfs://194.102.62.135:9000/user/mariano/");
			  File dir = new File(hdfs.getWorkingDirectory().toString());
			  
			  FileStatus[] statuses = hdfs.listStatus(new Path("/tmp"));
			  for(FileStatus status: statuses )
			  {
				  String name = status.getPath().getName();
				  String path = status.getPath().toString();
				  String parent = status.getPath().getParent().toString();
				  
				  boolean isDir = status.isDir();
				  Long length = status.getLen();
				  
				  LsElement el = new LsElement(name, path, isDir, length);
				  
				  System.out.println("Name : " + el.getName());
				  System.out.println("path : " + el.getPath());
				  System.out.println("Parent : " + el.getParent());
				  System.out.println("isDir : " + el.isDirectory());
				  System.out.println("length : " + el.getLength());
			  }
			  
		  }catch (Exception e){
			  System.out.println(e.getMessage());
		  }
		
	}
	public static void main(String [ ] args) throws IOException, URISyntaxException{
		
//		try{
			URI uri = new URI("hdfs://localhost:50070/");
			config.set("fs.default.name", "hdfs://localhost:9000");
//			config.set("namenode", "hdfs://194.102.62.135:50070/");
//			config.set("namenode", "hdfs://194.102.62.135:9000/");
			
//			Iterator<Entry<String,String>> iter = config.iterator();
			
//			while(iter.hasNext())
//			{
//				Entry<String,String> entry = iter.next();
//				System.out.println(entry.getKey() + "\t:" + entry.getValue());
//			}
			
			
			hdfs = FileSystem.get(config);
			
			System.out.println( hdfs.getCanonicalServiceName());
			System.out.println( hdfs.getWorkingDirectory());
			hdfs.printStatistics();
			
			
//			hdfs.rename(new Path("hdfs://194.102.62.135:9000/user/Boris/"),
//						new Path("hdfs://194.102.62.135:9000/user/mariano/"));
//			hdfs = FileSystem.get(config);
//			hdfs.printStatistics();
			

//			testListStatus();
//			testMkdir();
//			testLs();
//			System.out.println(filePath.toString() + " deletion = " + testDelete());
//			System.out.println(filePath.toString() + " copied = " + testCopy());
//			testMkdir();
//			testRmdir();
//			testWrite();
//			testCopy();
//		}catch (Exception e){
//			  System.out.println(e.getMessage());
//			  System.out.println(e.getStackTrace().clone());
//		}
		
//		testListStatus();
		testWrite();
		
	}
}
