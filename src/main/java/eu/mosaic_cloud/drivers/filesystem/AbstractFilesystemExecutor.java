/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem;

import org.apache.hadoop.conf.Configuration;
import org.slf4j.Logger;

import eu.mosaic_cloud.platform.core.ops.IOperationType;
import eu.mosaic_cloud.tools.transcript.core.Transcript;

public abstract class AbstractFilesystemExecutor {

	protected Configuration config = new Configuration();
	protected Transcript transcript = Transcript.create (this, true);
	final protected Logger logger = this.transcript.adaptAs (Logger.class);
	
   
    public abstract void destroy ();
    
	public abstract Object invokeOperation(final IOperationType type, Object... parameters);

}
