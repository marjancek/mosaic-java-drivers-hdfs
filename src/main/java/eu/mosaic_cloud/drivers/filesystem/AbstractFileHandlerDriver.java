/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem;

import java.io.IOException;
import java.util.UUID;

import eu.mosaic_cloud.drivers.AbstractResourceDriver;
import eu.mosaic_cloud.platform.core.ops.IOperationType;
import eu.mosaic_cloud.tools.threading.core.ThreadingContext;

public abstract class AbstractFileHandlerDriver extends AbstractResourceDriver{
	protected final AbstractFileHandlerExecutor executor;
    protected final ThreadingContext threading;
	private final String handler = UUID.randomUUID().toString();

    public AbstractFileHandlerDriver(ThreadingContext threading, int noThreads, Object... params) throws IOException {
        super(threading, noThreads);
         this.threading = threading;
         this.executor = AbstractFileHandlerDriver.this.createExecutor(params);
	}
    
    protected abstract AbstractFileHandlerExecutor createExecutor(Object... params) throws IOException;
    
    public Boolean close()
    {
    	return (Boolean) executor.invokeOperation(FileHandlerOperantions.CLOSE);
    }
    
  
    public byte[] read(long chunkSize)
    {
    	return (byte[]) executor.invokeOperation(FileHandlerOperantions.READ, chunkSize);
    }

    
    public Boolean write(byte[] data)
    {
    	return (Boolean) executor.invokeOperation(FileHandlerOperantions.WRITE, data, 0, data.length);
    }
    
    public Long write(byte[] data, int offset, int length)
    {
    	return (Long) executor.invokeOperation(FileHandlerOperantions.WRITE, data, offset, length);
    }

    public Boolean seek(Long position)
    {
    	return (Boolean) executor.invokeOperation(FileHandlerOperantions.SEEK, position);
    }
    
    public Boolean flush()
    {
    	return (Boolean) executor.invokeOperation(FileHandlerOperantions.FLUSH);
    }
    
    public Object unknownOperation(IOperationType type, Object... parameters) {
    	return executor.invokeOperation(type,parameters);
    }
   
	public String getHandle() {
		return handler;
	}
}
/*

class MyReadNHandler<T extends Object> implements IOperationCompletionHandler<T>{

	private int totalBytes;
	private int chunkSize;
	private IOperationCompletionHandler<byte[]> handler;
	private AbstractFileHandlerDriver driver;
	
	public MyReadNHandler(int totalBytes, int chunkSize, 
			IOperationCompletionHandler<byte[]> handler, AbstractFileHandlerDriver driver){
		
		super();
		this.totalBytes = totalBytes;
		this.chunkSize = chunkSize;
		this.handler = handler;
		this.driver = driver;
	}
	
	
	@Override
	public void onFailure(Throwable error) {
		handler.onFailure(error); //pass the error to handler
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onSuccess(T result) {
		
		byte[] bytes = null;
		
		if(result != null){
			bytes = (byte[]) result;
		}
		
		if(bytes!=null && bytes.length>0)	{
			handler.onSuccess(bytes);
			totalBytes = totalBytes - bytes.length;
			if(totalBytes > 0){
				driver.invokeReadOperation(chunkSize, (IOperationCompletionHandler<byte[]>) this);
			}else{
				handler.onSuccess(null);
			}
		}else{
			handler.onSuccess(null);
		}
		
	}	
	
}
*/
