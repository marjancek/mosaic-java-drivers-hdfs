/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem.hdfs;

import java.io.IOException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import eu.mosaic_cloud.drivers.filesystem.AbstractFileHandlerDriver;
import eu.mosaic_cloud.drivers.filesystem.AbstractFileHandlerExecutor;
import eu.mosaic_cloud.platform.interop.idl.dfs.DFSPayloads;
import eu.mosaic_cloud.tools.threading.core.ThreadingContext;
/**
 * Driver class for HDFSFileDriver 
 *
 */
public class HDFSFileDriver extends AbstractFileHandlerDriver{

	/**
	 * Creates new HDFSFileDriver
	 * 
	 * @param threading
	 * @param noThreads
	 * @param hdfs 
	 * 			FileSystem driver
	 * @param path
	 * 			path to a file
	 * @param mode
	 * 			defines a mode from FilesystemModes
 	* @throws Exception
 	*/
	HDFSFileDriver(ThreadingContext threading, int noThreads,
			FileSystem hdfs, String path, DFSPayloads.OpenFile.FileOpenMode mode) throws Exception {
		super(threading, noThreads, hdfs, path, mode);

	}
	
	@Override
	protected AbstractFileHandlerExecutor createExecutor(Object... params) throws IOException {
		
		//FileSystem hdfs, Path path, DFSPayloads.OpenFile.FileOpenMode mode
		return HDFSHandlerExecutor.createExecutor((FileSystem)params[0],(String)params[1],(DFSPayloads.OpenFile.FileOpenMode)params[2]);
	}




}

