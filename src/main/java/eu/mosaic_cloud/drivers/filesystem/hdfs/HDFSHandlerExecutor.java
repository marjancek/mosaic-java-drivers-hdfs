/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem.hdfs;

import java.io.IOException;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import eu.mosaic_cloud.drivers.filesystem.AbstractFileHandlerExecutor;
import eu.mosaic_cloud.drivers.filesystem.FileHandlerOperantions;
import eu.mosaic_cloud.drivers.filesystem.FilesystemOperations;
import eu.mosaic_cloud.platform.core.ops.IOperationType;
import eu.mosaic_cloud.platform.interop.idl.dfs.DFSPayloads;

public class HDFSHandlerExecutor  extends AbstractFileHandlerExecutor{
	
	protected FileSystem hdfs;
	protected DFSPayloads.OpenFile.FileOpenMode mode;
	protected Path path;
	protected FSDataInputStream istream=null;
	protected FSDataOutputStream ostream=null;
	protected int bufferSize;
	protected byte[] buffer;
	
	public HDFSHandlerExecutor(FileSystem hdfs, Path path, DFSPayloads.OpenFile.FileOpenMode mode, int bufferSize) throws IOException{
		initialize(hdfs, path, mode, bufferSize);
	}
	
	public HDFSHandlerExecutor(FileSystem hdfs, Path path, DFSPayloads.OpenFile.FileOpenMode mode) throws Exception
	{
		initialize(hdfs, path, mode, 1024*1024);
	}
	
	public HDFSHandlerExecutor(FileSystem hdfs, String fullpath, DFSPayloads.OpenFile.FileOpenMode mode) throws IOException
	{
		path = new Path(fullpath);
		initialize(hdfs, path, mode, 1024*1024);
	}
	
    public static HDFSHandlerExecutor createExecutor(FileSystem hdfs, String path, DFSPayloads.OpenFile.FileOpenMode mode) throws IOException {
        return new HDFSHandlerExecutor(hdfs, path, mode);
    }
	
	private synchronized void initialize(FileSystem hdfs, Path path, DFSPayloads.OpenFile.FileOpenMode mode, int bufferSize) throws IOException
	{
		this.hdfs = hdfs;
		this.mode = mode;
		this.path = path;
		this.bufferSize = bufferSize;
		
		
		if (mode == DFSPayloads.OpenFile.FileOpenMode.READ_SEEKABLE || 
				mode == DFSPayloads.OpenFile.FileOpenMode.READ_SEQUENTIAL)
		{
			istream = hdfs.open(path);
		}
		else if (mode == DFSPayloads.OpenFile.FileOpenMode.WRITE_SEEKABLE)
		{
			ostream = hdfs.append(path);
		}
		else if (mode == DFSPayloads.OpenFile.FileOpenMode.OVERWRITE_SEQUENTIAL)
		{
			ostream = hdfs.create(path, true);
		}
		buffer = new byte[bufferSize];
	}
	
	@Override
	public synchronized void destroy() {
		try {
			if(ostream!=null)
			{
				ostream.flush();
				ostream.close();
			}
			if(istream!=null)
			{
				istream.close();
			}
			
		} catch (IOException e) {
			logger.warn("Exception closing file", e);
		}
	}
	
	@Override
	public synchronized Object invokeOperation(IOperationType type, Object... parameters) 
	{
		if (!(type instanceof FileHandlerOperantions)) {
			logger.error("Unsuported operation", type.toString());
			throw new UnsupportedOperationException(type.toString());
		}
		
		Object result;
		
		final FileHandlerOperantions mType = (FileHandlerOperantions) type;
		
		switch (mType) {
		case READ:	
			result = doRead(parameters); 
        	break;
		case WRITE:	
			result = doWrite(parameters); 
        	break;
		case SEEK:
			result = null;
			logger.error("Unsuported operation", type.toString());
			// throw new UnsupportedOperationException(type.toString());
			break;
		case FLUSH:
			result = null;
			logger.error("Unsuported operation", type.toString());
			// throw new UnsupportedOperationException(type.toString());
			break;
		case CLOSE:
			result = doClose(parameters);
			break;
		default:
			result = null;
			logger.error("Unsuported operation", type.toString());
			// throw new UnsupportedOperationException(type.toString());
		}
		
		return result;
	}
	

	private byte[] doRead(final Object... parameters)
    {
    	byte[] result; 
    	if(parameters[0] != null && (Long)parameters[0] > 0){
    		long chunkSize = (Long)parameters[0];
    		result = new byte[(int)chunkSize];
    	}else {
    		result = new byte[bufferSize];
    	}

    	try{
    		int read=0;
    		read = istream.read(result);
    		if(read<=0)
    			result=null;
    		else if ( read < result.length)
    		{
    			byte[] copy = new byte[read];
    			System.arraycopy(result, 0, copy, 0, read);
    			result = copy;
    		}
    		logger.info("Read "+read+" bytes from file" );//+ (String) path.toString());
		} catch (Exception e) {
			logger.warn("Exception reading file" + (String) path.getName(), e);
		}
    	return result;
    }

    private Boolean doWrite(Object[] parameters) {
    	Boolean success = false;
    	if(!(parameters[0] instanceof byte[]))
    		return false;
    	
    	byte[] buffer = (byte[]) parameters[0];
    	
    	int offset = 0;
    	int length = buffer.length;
    	
    	if(parameters[1]!=null && parameters[1] instanceof Integer &&
    			parameters[2]!=null && parameters[2] instanceof Integer)
    	{
    		offset = (Integer) parameters[1];
    		length = (Integer) parameters[2];
    	}
    	
        try{
        		ostream.write(buffer, offset, length);
        		logger.info("Written "+length+" bytes to file " + (String) path.getName());
        		success=true;
        } catch (IOException e) {
				logger.warn("Exception closing file" + (String) path.getName(), e);
		}
        	return success;
	}
    
	private Object doClose(Object[] parameters) {
    	Boolean success = false;
    	try {
			if(ostream!=null)
			{
				ostream.flush();
				ostream.close();
			}
			if(istream!=null)
			{
				istream.close();
			}
			success=true;
		} catch (IOException e) {
			logger.warn("Exception closing file" + (String) path.getName(), e);
		}
    	return success;
	}
}
