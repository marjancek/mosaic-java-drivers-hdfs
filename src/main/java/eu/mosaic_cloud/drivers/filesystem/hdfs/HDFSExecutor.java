/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem.hdfs;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;

import eu.mosaic_cloud.drivers.filesystem.AbstractFilesystemExecutor;
import eu.mosaic_cloud.drivers.filesystem.FilesystemOperations;
import eu.mosaic_cloud.drivers.filesystem.LsElement;
import eu.mosaic_cloud.platform.core.ops.IOperationType;
import eu.mosaic_cloud.platform.interop.idl.dfs.DFSPayloads;
import eu.mosaic_cloud.tools.threading.core.ThreadingContext;

public class HDFSExecutor extends AbstractFilesystemExecutor{

	protected FileSystem hdfs;
	protected int bufferSize;
	
	private HDFSExecutor(String host, int port) throws IOException
	{
		config.set("fs.default.name", "hdfs://"+host+":"+port+"/");
		hdfs = FileSystem.get(config);
//		this.transcript = Transcript.create (this, true);
	}
	
	static public HDFSExecutor createExecutor(String host, int port) throws IOException {
		return new HDFSExecutor(host, port); 
	}
		
	
	@Override
	public synchronized void destroy() {
		try {
			hdfs.close();
		} catch (IOException e) {
			logger.warn("Exception closing file system", e);
		}
	}

	@Override
	public synchronized Object invokeOperation(IOperationType type, Object... parameters) 
	{
		Object result;
		
		if (!(type instanceof FilesystemOperations)) {
            throw new UnsupportedOperationException(type.toString());
		}
		
		final FilesystemOperations mType = (FilesystemOperations) type;
		
		switch (mType) {
		case COPY:	
			result = doCopy(parameters); 
        	break;
		case REMOVE_FILE:	
			result = doRemoveFile(parameters); 
        	break;
		case REMOVE_DIR:
			result = doRemoveDirectory(parameters);
			break;
		case MOVE:
			result = doMove(parameters);
			break;
		case OPEN:
			result = doOpen(parameters);
			break;
		case MKDIR:
			result = doMakeDir(parameters);
			break;
		case LIST:
			result = doList(parameters);
			break;
		default:
			result = null;
			logger.error("UnsupportedOperationException " + type.toString());
			// throw new UnsupportedOperationException(type.toString());
		}
		return result;
	}


	private Boolean doCopy(Object[] parameters) {
    	Boolean success = false;
    	try {
    		success = FileUtil.copy(
    				hdfs, constructPath( parameters[0]),
    				hdfs, constructPath( parameters[1]),
    				false, false, this.config
    				);
    	
		} catch (IOException e) {
			logger.warn("IOException Copying file " + (String) parameters[0] +
					" to " + (String) parameters[1], e);
		}catch (Exception e) {
			logger.warn("Exception Copying file " + (String) parameters[0] +
					" to " + (String) parameters[1], e);
		}catch (Throwable th) {
			logger.error("Thrown exception while copying file " + (String) parameters[0] +
					" to " + (String) parameters[1], th);
		}
        return success;
	}

	private Boolean doRemoveFile(Object[] parameters) {
    	Boolean success = false;
    	try {
			success = hdfs.delete(constructPath( parameters[0]) , false);
		} catch (IOException e) {
			logger.warn("Exception removing file " + (String) parameters[0], e);
		}
        return success;
	}
	
	private Object doRemoveDirectory(Object[] parameters) {
    	Boolean success = false;
    	try {
			success = hdfs.delete(constructPath( parameters[0]) , true);
		} catch (IOException e) {
			logger.warn("Exception removing directory " + (String) parameters[0], e);
		}
        return success;
	}
	
	private HDFSFileDriver doOpen(Object[] parameters) {
    	HDFSFileDriver handler = null;
    	try {
			
    		handler = new HDFSFileDriver((ThreadingContext) parameters[0],
    				1, hdfs, (String)parameters[1], (DFSPayloads.OpenFile.FileOpenMode) parameters[2]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.warn("Exception OPENING FILE " + (String) parameters[1], e);
		}
    	
        return handler;
	}

	private Boolean doMove(Object[] parameters) {
    	Boolean success = false;
    	try {
			success = hdfs.rename(constructPath( parameters[0]) , constructPath( parameters[1]));
		} catch (IOException e) {
			logger.warn("Exception moving directory/file from " + (String) parameters[0] +
						" to "+ (String) parameters[1], e);
		}
        return success;
	}

	private Boolean doMakeDir(Object[] parameters) {
    	Boolean success = false;
    	try {
    		Path p = constructPath(parameters[0]);
			success = hdfs.mkdirs(p);
		} catch (IOException e) {
			logger.warn("Exception creating new directory " + (String) parameters[0]);
		}
        return success;
	}

	private List<LsElement> doList(Object[] parameters) {
    	List<LsElement> elements=null;
    	
    	try {
        	elements = new ArrayList<LsElement>();
        	
        	FileStatus[] statuses = hdfs.listStatus(constructPath(parameters[0]));
			
        	for(FileStatus status: statuses )
        	{
					String name = status.getPath().getName();
					String path = status.getPath().toString();
			  
					boolean isDir = status.isDir();
					Long length = status.getLen();
					  
					LsElement el = new LsElement(name, path, isDir, length);
					elements.add(el);
			  }	            
    	}catch(Exception e){
    		logger.warn("Exception while listing directory " + (String) parameters[0]);
    	}
    	
    	return elements;
	}
	
    private Path constructPath(Object obj)
    {
    	Path p;
    	
    	if(obj instanceof URI)
    		p = new Path((URI) obj);
    	else if (obj instanceof Path)
    		p = (Path) obj;
    	else if (obj instanceof String)
    		p = new Path((String) obj);
    	else
    		p = new Path((String) obj.toString());
    	
    	return p;
    }
	

}
