/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem.hdfs;

import java.io.IOException;

import eu.mosaic_cloud.drivers.filesystem.AbstractFilesystemDriver;
import eu.mosaic_cloud.drivers.filesystem.AbstractFilesystemExecutor;
import eu.mosaic_cloud.platform.core.configuration.ConfigUtils;
import eu.mosaic_cloud.platform.core.configuration.IConfiguration;
import eu.mosaic_cloud.tools.threading.core.ThreadingContext;
/**
 * Driver class for HDFS distributed file system.
 * 
 */
public class HDFSDriver extends AbstractFilesystemDriver
{

	/**
	 * Creates a new HDFS driver
	 * @param threading
	 * @param noThreads
	 * 			number of threads used for serving requests
	 * @param host
	 * 			hostname of the HDFS server 
	 * @param port
	 * 			port of the HDFS server
	 * @throws IOException
	 */
	public HDFSDriver(ThreadingContext threading, Integer noThreads, String host, Integer port) throws IOException 
	{
		super(threading, noThreads, host, port);
	}

	/**
	 * Returns the HDFSDriver
	 * 
	 * @param config
	 * 			the configuration parameters required by the driver
	 * @param threading
	 * @return the driver
	 * @throws IOException
	 */
    public static HDFSDriver create(IConfiguration config, ThreadingContext threading)
            throws IOException {
        String host = ConfigUtils.resolveParameter(config,"dfs.host", String.class, "localhost"); 
        int port = ConfigUtils.resolveParameter(config, "dfs.port", Integer.class, 9000);
        int noThreads = ConfigUtils.resolveParameter(config, "dfs.threads", Integer.class, 1); 
                
        return new HDFSDriver(threading, noThreads, host, port);
        
    }
    
	@Override
	protected AbstractFilesystemExecutor createExecutor(Object... params)
			throws IOException {
		return HDFSExecutor.createExecutor((String)params[0],(Integer)params[1]);
	}

   

}
