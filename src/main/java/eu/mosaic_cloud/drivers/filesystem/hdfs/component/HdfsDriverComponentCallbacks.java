/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package eu.mosaic_cloud.drivers.filesystem.hdfs.component;


import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import eu.mosaic_cloud.components.core.ComponentAcquireReply;
import eu.mosaic_cloud.components.core.ComponentCallReference;
import eu.mosaic_cloud.components.core.ComponentCallReply;
import eu.mosaic_cloud.components.core.ComponentCallRequest;
import eu.mosaic_cloud.components.core.ComponentController;
import eu.mosaic_cloud.components.core.ComponentEnvironment;
import eu.mosaic_cloud.components.core.ComponentIdentifier;
import eu.mosaic_cloud.drivers.component.AbstractDriverComponentCallbacks;
import eu.mosaic_cloud.drivers.filesystem.interop.DFSInstanceStub;
import eu.mosaic_cloud.interoperability.implementations.zeromq.ZeroMqChannel;
import eu.mosaic_cloud.platform.core.configuration.ConfigUtils;
import eu.mosaic_cloud.platform.core.configuration.IConfiguration;
import eu.mosaic_cloud.platform.core.configuration.PropertyTypeConfiguration;
import eu.mosaic_cloud.platform.interop.specs.dfs.DFSSession;
import eu.mosaic_cloud.tools.callbacks.core.CallbackCompletion;

import com.google.common.base.Preconditions;


public final class HdfsDriverComponentCallbacks
		extends AbstractDriverComponentCallbacks
{
	public HdfsDriverComponentCallbacks (final ComponentEnvironment context)
	{
		super (context);
		try {
			final PropertyTypeConfiguration configuration = PropertyTypeConfiguration.create (HdfsDriverComponentCallbacks.class.getResourceAsStream ("driver-component.properties"));
			if (context.supplementary.containsKey ("hdfs.host"))
				configuration.addParameter ("dfs.host", context.supplementary.get ("hdfs.host"));
			if (context.supplementary.containsKey ("hdfs.port"))
				configuration.addParameter ("dfs.port", context.supplementary.get ("hdfs.port"));
			this.setDriverConfiguration (configuration);
			this.selfGroup = ComponentIdentifier.resolve (ConfigUtils.resolveParameter (this.getDriverConfiguration (), "self.group.identifier", String.class, ""));
			this.status = Status.Created;
		} catch (final IOException e) {
			this.exceptions.traceIgnoredException (e);
		}
	}
	
	@Override
	public final CallbackCompletion<Void> acquireReturned (final ComponentController component, final ComponentAcquireReply reply)
	{
		throw (new IllegalStateException ());
	}
	
	@Override
	public CallbackCompletion<Void> called (final ComponentController component, final ComponentCallRequest request)
	{
		Preconditions.checkState (this.component == component);
		Preconditions.checkState ((this.status != Status.Terminated) && (this.status != Status.Unregistered));
		if (this.status == Status.Registered) {
			if (request.operation.equals ("mosaic-component:get.channel.data")) {
				String channelEndpoint = ConfigUtils.resolveParameter (this.getDriverConfiguration (), "interop.channel.address", String.class, "");
				// FIXME: These parameters should be determined through
				//-- component "resource acquire" operations.
				//-- Also this hack reduces the number of driver instances of
				//-- the same type to one per VM.
				try {
					if (System.getenv ("mosaic_node_ip") != null) {
						channelEndpoint = channelEndpoint.replace ("0.0.0.0", System.getenv ("mosaic_node_ip"));
					} else {
						channelEndpoint = channelEndpoint.replace ("0.0.0.0", InetAddress.getLocalHost ().getHostAddress ());
					}
				} catch (final UnknownHostException e) {
					this.exceptions.traceIgnoredException (e);
				}
				final String channelId = ConfigUtils.resolveParameter (this.getDriverConfiguration (), "interop.driver.identifier", String.class, "");
				final Map<String, String> outcome = new HashMap<String, String> ();
				outcome.put ("channelEndpoint", channelEndpoint);
				outcome.put ("channelIdentifier", channelId);
				final ComponentCallReply reply = ComponentCallReply.create (true, outcome, ByteBuffer.allocate (0), request.reference);
				component.callReturn (reply);
			} else {
				throw new UnsupportedOperationException ();
			}
		} else {
			throw new UnsupportedOperationException ();
		}
		return null;
	}
	
	@Override
	public CallbackCompletion<Void> callReturned (final ComponentController component, final ComponentCallReply reply)
	{
		throw (new IllegalStateException ());
	}
	
	@Override
	public CallbackCompletion<Void> initialized (final ComponentController component)
	{
		Preconditions.checkState (this.component == null);
		Preconditions.checkState (this.status == Status.Created);
		this.component = component;
		final ComponentCallReference callReference = ComponentCallReference.create ();
		this.pendingReference = callReference;
		this.pendingReference = ComponentCallReference.create ();
		this.status = Status.Unregistered;
		this.component.register (this.selfGroup, this.pendingReference);
		this.logger.trace ("HDFS driver callback initialized.");
		return null;
	}
	
	@Override
	public CallbackCompletion<Void> registerReturned (final ComponentController component, final ComponentCallReference reference, final boolean registerOk)
	{
		Preconditions.checkState (this.component == component);
		if (this.pendingReference == reference) {
			if (!registerOk) {
				final Exception e = new Exception ("failed registering to group; terminating!");
				this.exceptions.traceDeferredException (e);
				this.component.terminate ();
				throw (new IllegalStateException (e));
			}
			this.logger.info ("HDFS driver callback registered to group " + this.selfGroup);
			this.status = Status.Registered;
			// NOTE: create stub and interop channel
			final String channelId = "interop.driver.identifier";
			final String channelEndpoint = "interop.channel.address";
			final ZeroMqChannel driverChannel = this.createDriverChannel (channelId, channelEndpoint, DFSSession.DRIVER);
			try {
				this.stub = DFSInstanceStub.create (this.getDriverConfiguration (), this.threading, driverChannel);
			} catch (Exception exception) {
				throw new Error (exception);
			}
		} else {
			throw new IllegalStateException ();
		}
		return null;
	}
	
	protected DFSInstanceStub stub;
}
