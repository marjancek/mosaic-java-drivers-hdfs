/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package eu.mosaic_cloud.drivers.filesystem;

/**
 * Class that holds data returned from method invokeListOperation()
 * 
 * @author Boris Savic
 *
 */
public class LsElement {
	
	private String name;
	private String path;
	private String parent;
	private boolean isDirectory;
	private Long length;
	
	/**
	 * Creates new LsElement. 
	 * 
	 * @param name
	 * 			name of the element
	 * @param path
	 * 			path to the element
	 * @param isDirectory
	 * 			is element a directory
	 * @param length
	 * 			size/length of the element
	 */
	public LsElement(String name, String path, boolean isDirectory, Long length){
		this.name = name;
		this.path = path;
		this.isDirectory = isDirectory;
		this.length = length;
		this.parent = path.substring(0, path.indexOf(name));
	}


	public String getName() {
		return name;
	}


	public String getPath() {
		return path;
	}


	public String getParent() {
		return parent;
	}


	public boolean isDirectory() {
		return isDirectory;
	}


	public Long getLength() {
		return length;
	}		
	
}

