/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem.interop;

import java.lang.reflect.Constructor;

import org.slf4j.Logger;

import eu.mosaic_cloud.drivers.filesystem.AbstractFileHandlerDriver;
import eu.mosaic_cloud.drivers.filesystem.AbstractFilesystemDriver;
import eu.mosaic_cloud.drivers.filesystem.FileHandlerDriverProxy;
import eu.mosaic_cloud.drivers.filesystem.FileHandlerOperantions;
import eu.mosaic_cloud.drivers.filesystem.FilesystemModes;
import eu.mosaic_cloud.drivers.filesystem.FilesystemOperations;
import eu.mosaic_cloud.interoperability.core.Message;
import eu.mosaic_cloud.interoperability.core.Session;
import eu.mosaic_cloud.interoperability.core.SessionCallbacks;
import eu.mosaic_cloud.interoperability.implementations.zeromq.ZeroMqChannel;
import eu.mosaic_cloud.platform.core.configuration.ConfigUtils;
import eu.mosaic_cloud.platform.core.configuration.IConfiguration;
import eu.mosaic_cloud.platform.core.ops.IOperationType;
import eu.mosaic_cloud.platform.interop.idl.IdlCommon.CompletionToken;
import eu.mosaic_cloud.platform.interop.idl.dfs.DFSPayloads;
import eu.mosaic_cloud.platform.interop.idl.dfs.DFSPayloads.OpenFile.FileOpenMode;
import eu.mosaic_cloud.platform.interop.specs.dfs.DFSHandlerMessage;
import eu.mosaic_cloud.platform.interop.specs.dfs.DFSMessage;
import eu.mosaic_cloud.platform.interop.specs.dfs.DFSSession;
import eu.mosaic_cloud.tools.callbacks.core.CallbackCompletion;
import eu.mosaic_cloud.tools.exceptions.core.FallbackExceptionTracer;
import eu.mosaic_cloud.tools.exceptions.tools.BaseExceptionTracer;
import eu.mosaic_cloud.tools.threading.core.ThreadingContext;
import eu.mosaic_cloud.tools.transcript.core.Transcript;

public class DFSInstanceStub implements SessionCallbacks {

	protected FileHandlerDriverProxy drivers;
	protected AbstractFilesystemDriver filesystem;
	private static final Logger logger = Transcript.create (DFSInstanceStub.class).adaptAs (Logger.class);
	protected final BaseExceptionTracer exceptions = FallbackExceptionTracer.defaultInstance;
	private final DFSResponseTransmitter transmitter = new DFSResponseTransmitter();
	
	public DFSInstanceStub (ThreadingContext threading, int noThreads, String host, int port,  final Class<? extends AbstractFilesystemDriver> driverClas) 
			throws Exception
	{
		Constructor<? extends AbstractFilesystemDriver> constructor = driverClas.getConstructor(ThreadingContext.class, Integer.class, String.class, Integer.class);
		AbstractFilesystemDriver parent = constructor.newInstance(threading, new Integer(noThreads), host,  new Integer(port));				
		init(parent);

		drivers = new FileHandlerDriverProxy();
	}
	
	@SuppressWarnings("unchecked")
	public static DFSInstanceStub create(final IConfiguration config, final ThreadingContext threadingContext, final ZeroMqChannel channel) throws Exception {
		
		DFSInstanceStub.logger.debug("Creating new DFSInstaceStub");
		
		String host = ConfigUtils.resolveParameter(config,"dfs.host", String.class, "127.0.0.1"); 
        int port = ConfigUtils.resolveParameter(config, "dfs.port", Integer.class, 9000);
        int noThreads = ConfigUtils.resolveParameter(config, "dfs.threads", Integer.class, 1); 
		String clasz = ConfigUtils.resolveParameter(config, "dfs.driver", String.class, "eu.mosaic_cloud.drivers.filesystem.hdfs.HDFSDriver");
		Class<? extends AbstractFilesystemDriver> claz = null;
		try {
			claz = (Class<? extends AbstractFilesystemDriver>) Class.forName(clasz);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		
		DFSInstanceStub stub =  new DFSInstanceStub(threadingContext, noThreads, host, port, claz);
		channel.accept(DFSSession.DRIVER, stub);
		
		return stub;
		
	}
	
	public DFSInstanceStub(AbstractFilesystemDriver parent) throws NullPointerException
	{
		init(parent);
	}
	
	protected void init(AbstractFilesystemDriver parent) throws NullPointerException
	{
		if(parent==null)
		{
			String msg = "Received invalid null AbstractFilesystemDriver";
			logger.error(msg);
			throw new NullPointerException(msg);
		}
		filesystem=parent;
	}
	
	@Override
	public CallbackCompletion<Void> created(Session session) {
		logger.error("Method should not be called!");
		return null;
	}

	@Override
	public CallbackCompletion<Void> destroyed(Session session) {
		logger.debug("DFSHandlerStub destroyed;");
		return null;
	}
	
	public void destroy(){
		logger.debug("DFSHandlerStub - cleaning up driver;");
		filesystem.destroy();
		filesystem=null;
	}

	@Override
	public CallbackCompletion<Void> failed(Session session, Throwable exception) {
		logger.debug("DFSHandlerStub failed; cleaning up driver");
		filesystem.destroy();
		filesystem=null;
		return null;
	}

	@Override
	public CallbackCompletion<Void> received(Session session, Message message) {
		logger.debug("DFSHandlerStub received message; processing...");
		
		if(message.specification instanceof DFSHandlerMessage)
			receivedHandlerMessage(session,  message);
		else if (message.specification instanceof DFSMessage)
			receivedFSMessage(session,  message);
				
		return null;
	}

	private void receivedFSMessage(Session session, Message message) {
		final DFSMessage type = (DFSMessage) message.specification;
		CompletionToken token = null;
		Object result;
		String path;
		String from;
		String to;
		
		switch (type) 
		{
		case ABORTED:
			DFSInstanceStub.logger.trace ("Recieved cancelation message");
			break;
		case ACCESS :
			DFSInstanceStub.logger.trace ("Received initiation message");
			break;
		case COPY:
				final DFSPayloads.CopyFile copyPayload = (DFSPayloads.CopyFile) message.payload;
				token = copyPayload.getToken();
				from =  copyPayload.getFromPath();
				to = copyPayload.getToPath();

				session.continueDispatch();
				result = filesystem.copyFile(from, to);
				sendResult(session, token, FilesystemOperations.COPY, result);
				
//				final IResult<Boolean> copyResult = filesystem.invokeCopyFileOperation(from , to, copyCallback);
//				copyCallback.setDetails (FilesystemOperations.COPY, copyResult);
				break;
				
			case LIST:
				final DFSPayloads.ListDir listPayload = (DFSPayloads.ListDir) message.payload;
				token = listPayload.getToken();
				path = listPayload.getFullPath();

				session.continueDispatch();
				result = filesystem.listDirectory(path);
				sendResult(session, token, FilesystemOperations.LIST, result);
				break;				
				
			case MKDIR:
				final DFSPayloads.MakeDir mkdirPayload = (DFSPayloads.MakeDir) message.payload;
				token = mkdirPayload.getToken();
				path  = mkdirPayload.getFullPath();

				session.continueDispatch();
				result = filesystem.makeDirectory(path);
				sendResult(session, token, FilesystemOperations.MKDIR, result);
				break;
				
			case MOVE:
				final DFSPayloads.MoveFile movePayload = (DFSPayloads.MoveFile) message.payload;
				token = movePayload.getToken();
				from =  movePayload.getFromPath();
				to = movePayload.getToPath();

				
				session.continueDispatch();
				result = filesystem.moveFile(from, to);
				sendResult(session, token, FilesystemOperations.MOVE, result);
				break;
				
			case OPEN:
				final DFSPayloads.OpenFile openPayload = (DFSPayloads.OpenFile) message.payload;
				token = openPayload.getToken();
				path =  openPayload.getFullPath();
				FileOpenMode mode = openPayload.getMode();

				
				
				session.continueDispatch();
				result = filesystem.openFile(path, translateMode(mode));
				if(result!=null && result instanceof AbstractFileHandlerDriver)
					DFSInstanceStub.this.drivers.register( (AbstractFileHandlerDriver)result );
				sendResult(session, token, FilesystemOperations.OPEN, result);
				break;
				
			case REMOVE_DIR:
				final DFSPayloads.RemoveDir rmdirPayload = (DFSPayloads.RemoveDir) message.payload;
				token = rmdirPayload.getToken();
				path  = rmdirPayload.getFullPath();

				session.continueDispatch();
				result = filesystem.removeDirectory(path);
				sendResult(session, token, FilesystemOperations.REMOVE_DIR, result);
				break;
				
			case REMOVE_FILE:
				final DFSPayloads.RemoveFile rmPayload = (DFSPayloads.RemoveFile) message.payload;
				token = rmPayload.getToken();
				path  = rmPayload.getFullPath();

				session.continueDispatch();
				result = filesystem.removeFile(path);
				sendResult(session, token, FilesystemOperations.REMOVE_FILE, result);
				break;
	
			default:
		}
	}


	private void receivedHandlerMessage(Session session, Message message) {
		
		final DFSHandlerMessage type = (DFSHandlerMessage) message.specification;
		CompletionToken token = null;
		AbstractFileHandlerDriver driver = null;
		Object result;
		
		switch (type) 
		{
			case READ:
				final DFSPayloads.ReadFile readPayload = (DFSPayloads.ReadFile) message.payload;
				token = readPayload.getToken();
				driver = drivers.get(readPayload.getFileHandler());
				
				session.continueDispatch();
				result = driver.read(readPayload.getNumBytes());
				sendResult(session, token, FileHandlerOperantions.READ, result);
				break;
				
			case WRITE:
				final DFSPayloads.WriteFile writePayload = (DFSPayloads.WriteFile) message.payload;
				token = writePayload.getToken();
				driver = drivers.get(writePayload.getFileHandler());
				
				session.continueDispatch();
				byte[] data = writePayload.getData().toByteArray();
				result = driver.write(data);
				sendResult(session, token, FileHandlerOperantions.WRITE, result);
				break;
				
			case FLUSH:
				
				final DFSPayloads.FlushFile flushPayload = (DFSPayloads.FlushFile) message.payload;
				token = flushPayload.getToken();
				driver = drivers.get(flushPayload.getFileHandler());
				result = driver.flush();
				sendResult(session, token, FileHandlerOperantions.FLUSH, result);
				break;
	
			case SEEK:
				final DFSPayloads.SeekFile seekPayload = (DFSPayloads.SeekFile) message.payload;
				token = seekPayload.getToken();
				driver = drivers.get(seekPayload.getFileHandler());
				
				result = driver.seek(seekPayload.getPosition());
				sendResult(session, token, FileHandlerOperantions.SEEK, result);
				break;
				
			case CLOSE:
				final DFSPayloads.CloseFile closePayload = (DFSPayloads.CloseFile) message.payload;
				token = closePayload.getToken();
				String handle = closePayload.getFileHandler();
				driver = drivers.get(handle);
				drivers.remove(handle);
				
				result = driver.close();
				driver.destroy();
				driver=null;
				sendResult(session, token, FileHandlerOperantions.CLOSE, result);
				break;
				
			default:
				//sendResult(session, null, null, new Exception("Unsupported Operation"));
		}
	
	}

	private FilesystemModes translateMode(FileOpenMode mode) {
		switch(mode)
		{
			case OVERWRITE_SEQUENTIAL:
				return FilesystemModes.OVERWRITE_SEQUENTIAL;
			case READ_SEEKABLE:
				return FilesystemModes.READ_SEEKABLE;
			case READ_SEQUENTIAL:
				return FilesystemModes.READ_SEQUENTIAL;
			case READ_WRITE_SEEKABLE:
				return FilesystemModes.READ_WRITE_SEEKABLE;
			case WRITE_SEEKABLE:
				return FilesystemModes.WRITE_SEEKABLE;
		}
		return null;
	}
	
	protected void sendResult(Session session, final CompletionToken token, final IOperationType operation, Object response)
	{
		if(response==null)
		{
			DFSInstanceStub.this.transmitter.sendResponse (session, token, operation, 
					"Error in " + operation.toString() + ")", true);
		} else if(response instanceof Exception){
			DFSInstanceStub.this.transmitter.sendResponse (session, token, operation, ((Exception) response).getMessage(), true);
		}else {
			DFSInstanceStub.this.transmitter.sendResponse (session, token, operation, response, false);
		}
	}
	
}
