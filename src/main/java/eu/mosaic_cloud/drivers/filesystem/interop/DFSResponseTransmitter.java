/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem.interop;

import java.util.List;

import com.google.protobuf.ByteString;

import eu.mosaic_cloud.drivers.filesystem.FileHandlerOperantions;
import eu.mosaic_cloud.drivers.filesystem.FilesystemOperations;
import eu.mosaic_cloud.drivers.filesystem.LsElement;
import eu.mosaic_cloud.drivers.filesystem.hdfs.HDFSFileDriver;
import eu.mosaic_cloud.drivers.interop.ResponseTransmitter;
import eu.mosaic_cloud.interoperability.core.Message;
import eu.mosaic_cloud.interoperability.core.Session;
import eu.mosaic_cloud.platform.core.ops.IOperationType;
import eu.mosaic_cloud.platform.interop.idl.IdlCommon;
import eu.mosaic_cloud.platform.interop.idl.IdlCommon.CompletionToken;
import eu.mosaic_cloud.platform.interop.idl.IdlCommon.Error;
import eu.mosaic_cloud.platform.interop.idl.dfs.DFSPayloads;
import eu.mosaic_cloud.platform.interop.idl.dfs.DFSPayloads.FileHandlerResponse;
import eu.mosaic_cloud.platform.interop.idl.dfs.DFSPayloads.FileRead;
import eu.mosaic_cloud.platform.interop.idl.dfs.DFSPayloads.ListResult;
import eu.mosaic_cloud.platform.interop.idl.dfs.DFSPayloads.ListingItem;
import eu.mosaic_cloud.platform.interop.idl.dfs.DFSPayloads.SuccessResponse;
import eu.mosaic_cloud.platform.interop.specs.dfs.DFSHandlerMessage;
import eu.mosaic_cloud.platform.interop.specs.dfs.DFSMessage;
import eu.mosaic_cloud.platform.interop.specs.kvstore.KeyValueMessage;

public class DFSResponseTransmitter extends ResponseTransmitter
{

	protected Message buildOperationResponse (final FilesystemOperations operation, final CompletionToken token, final Object result)
	{
		Message message = null;
		switch (operation) {
			case COPY:		
			case REMOVE_FILE:
			case REMOVE_DIR:
			case MOVE:
			case LINK:
			case MKDIR:
			case CLOSE:
					
				final boolean success = (Boolean) result;
				final SuccessResponse.Builder successPayload = DFSPayloads.SuccessResponse.newBuilder();
				successPayload.setToken (token);
				successPayload.setSuccessful(success);
				message = new Message (DFSMessage.SUCCESS, successPayload.build());
				
				break;
				
			case LIST:
				@SuppressWarnings("unchecked")
				final List<LsElement> list = (List<LsElement>) result; 
				final ListResult.Builder lsPayload = DFSPayloads.ListResult.newBuilder();
				lsPayload.setToken(token);
				

				int i = 1; 
				for(LsElement element:list)
				{
					ListingItem.Builder builder = DFSPayloads.ListingItem.newBuilder();
					
					builder.setFullPath(element.getPath());
					builder.setIsDirectory(element.isDirectory());
					builder.setLength(element.getLength());
					builder.setName(element.getName());
					lsPayload.addEntry(builder.build());
					i++;
				}
				message = new Message(DFSMessage.LISTING, lsPayload.build()); 
				break;
				
			case OPEN:
				HDFSFileDriver driver = (HDFSFileDriver) result;
				FileHandlerResponse.Builder handler = FileHandlerResponse.newBuilder(); 
				handler.setToken(token);
				handler.setFileHandler(driver.getHandle());
				message = new Message(DFSMessage.HANDLER, handler.build());
				break;
			default:
				break;
		}
		
		return message;
	}
	
	
	protected Message buildHandlerResponse (final FileHandlerOperantions operation, final CompletionToken token, final Object result)
	{
		Message message = null;
		switch (operation) {
			case WRITE:
			case CLOSE:
			case SEEK:
			case FLUSH:
				final boolean success = (Boolean) result;
				final SuccessResponse.Builder successPayload = DFSPayloads.SuccessResponse.newBuilder();
				successPayload.setToken (token);
				successPayload.setSuccessful(success);
				message = new Message (DFSHandlerMessage.SUCCESS, successPayload.build());
				
				break;
			case READ:
				
				final byte[] bytes = (byte[]) result;
				final FileRead.Builder readPayload = FileRead.newBuilder();
				readPayload.setToken(token);
				ByteString value = ByteString.copyFrom(bytes);
				readPayload.setData(value);
				message = new Message (DFSHandlerMessage.BYTES, readPayload.build());
				
				break;
			default:
				break;
		}
		return message;
	}
	
	protected Message buildResponse (final IOperationType operation, final CompletionToken token, final Object result)
	{

		if (operation instanceof FileHandlerOperantions)
			return buildHandlerResponse((FileHandlerOperantions) operation, token, result);
		else if (operation instanceof FilesystemOperations)
			return buildOperationResponse((FilesystemOperations) operation, token, result);
		return null;
	}

	protected void sendResponse (final Session session, final CompletionToken token, final IOperationType operation, final Object result, final boolean isError)
	{
		Message message;
		if(token!=null)
			this.logger.trace ("DFSResponseTransmitter: send response for " + operation + " request " + token.getMessageId () + " client id " + token.getClientId ());
		else
			this.logger.trace ("DFSResponseTransmitter: send response for " + operation + " request " + ", null token");
		if (isError) {
			// NOTE: create error message
			final Error.Builder errorPayload = IdlCommon.Error.newBuilder ();
			errorPayload.setToken (token);
			if(result!=null)
				errorPayload.setErrorMessage (result.toString ());
			else
				errorPayload.setErrorMessage ("Unknown error");
			message = new Message (DFSMessage.ERROR, errorPayload.build ());
		} else {
			message = this.buildResponse (operation, token, result);
		}
		// NOTE: send response
		this.publishResponse (session, message);
	}
}

