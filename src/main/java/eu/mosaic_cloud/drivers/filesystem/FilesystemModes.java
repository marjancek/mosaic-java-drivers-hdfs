/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem;

import eu.mosaic_cloud.platform.interop.idl.dfs.DFSPayloads.OpenFile.FileOpenMode;

public enum FilesystemModes  {
	READ_SEQUENTIAL, READ_SEEKABLE,
	OVERWRITE_SEQUENTIAL, 
	WRITE_SEEKABLE,
	READ_WRITE_SEEKABLE;
	
	public static FilesystemModes translateMode(FileOpenMode mode) {
		switch(mode)
		{
			case OVERWRITE_SEQUENTIAL:
				return FilesystemModes.OVERWRITE_SEQUENTIAL;
			case READ_SEEKABLE:
				return FilesystemModes.READ_SEEKABLE;
			case READ_SEQUENTIAL:
				return FilesystemModes.READ_SEQUENTIAL;
			case READ_WRITE_SEEKABLE:
				return FilesystemModes.READ_WRITE_SEEKABLE;
			case WRITE_SEEKABLE:
				return FilesystemModes.WRITE_SEEKABLE;
		}
		return null;
	}
	
	public FileOpenMode translateMode()
	{
		switch(this)
		{
			case OVERWRITE_SEQUENTIAL:
				return FileOpenMode.OVERWRITE_SEQUENTIAL;
			case READ_SEEKABLE:
				return FileOpenMode.READ_SEEKABLE;
			case READ_SEQUENTIAL:
				return FileOpenMode.READ_SEQUENTIAL;
			case READ_WRITE_SEEKABLE:
				return FileOpenMode.READ_WRITE_SEEKABLE;
			case WRITE_SEEKABLE:
				return FileOpenMode.WRITE_SEEKABLE;
		}
		return null;
	}
	
}
