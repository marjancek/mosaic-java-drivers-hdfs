/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem;

import java.io.IOException;
import java.util.List;

import eu.mosaic_cloud.drivers.IResourceDriver;
import eu.mosaic_cloud.platform.core.ops.IOperationType;
import eu.mosaic_cloud.tools.threading.core.ThreadingContext;

/**
 * @author Mariano Cecowski
 * 
 * Abstract diver for the access to a filesystem that allows for operations such 
 * as listing directories, removing, moving and copying files and directories, 
 * and creating drivers for the access to files (AbstractFileHandlerDriver) 
 * 
 */
public abstract class AbstractFilesystemDriver implements IResourceDriver {//  extends AbstractResourceDriver {
    protected AbstractFilesystemExecutor executor;
    protected final ThreadingContext threading;
    
	protected AbstractFilesystemDriver(ThreadingContext threading, int noThreads, Object... params) throws IOException {
//		super(threading, noThreads);
		
		this.threading = threading;
        this.executor = AbstractFilesystemDriver.this.createExecutor(params);

	}
    
    protected abstract AbstractFilesystemExecutor createExecutor(Object... params) throws IOException;
    
//    private AbstractFilesystemExecutor getexecutor() {
//        return this.executor;
//    }
	
	@Override
	public void destroy() {
		executor.destroy();
		executor=null;
	}
	
    public Boolean removeFile(String fullPath)
    {
    	return (Boolean) executor.invokeOperation(FilesystemOperations.REMOVE_FILE, fullPath);
    }
    
    public Boolean removeDirectory(String fullPath)
    {
    	return (Boolean) executor.invokeOperation(FilesystemOperations.REMOVE_DIR, fullPath);
    }
    
    public Boolean copyFile(String sourceFullPath, String destinationFullPath)
    {
    	return (Boolean) executor.invokeOperation(FilesystemOperations.COPY, sourceFullPath, destinationFullPath);
    }

    public Boolean moveFile(String sourceFullPath, String destinationFullPath)
    {
    	return (Boolean) executor.invokeOperation(FilesystemOperations.MOVE, sourceFullPath, destinationFullPath);
    }
    
    public Boolean makeDirectory(String dirNameFullPath)
    {
    	return (Boolean) executor.invokeOperation(FilesystemOperations.MKDIR, dirNameFullPath);
    }
    
    @SuppressWarnings("unchecked")
	public List<LsElement> listDirectory(String path)
    {
        return (List<LsElement>) executor.invokeOperation(FilesystemOperations.LIST, path);
    }
    
    public AbstractFileHandlerDriver openFile(String path, FilesystemModes mode)
    {
        return (AbstractFileHandlerDriver) executor.invokeOperation(FilesystemOperations.OPEN, threading, path, mode.translateMode());
    }
    

    public Object unknownOperation(IOperationType type, Object... parameters) {
    	return executor.invokeOperation(type,parameters);
    }

    
}
