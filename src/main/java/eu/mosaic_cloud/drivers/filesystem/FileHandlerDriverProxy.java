/*
 * #%L
 * mosaic-drivers-stubs-hdfs
 * %%
 * Copyright (C) 2010 - 2012 XLAB (Ljubljana, Slovenia)
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package eu.mosaic_cloud.drivers.filesystem;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class FileHandlerDriverProxy {
	
	protected Map<String, AbstractFileHandlerDriver> map;
	
	public FileHandlerDriverProxy()
	{
		 map = new ConcurrentHashMap<String, AbstractFileHandlerDriver>();
	}

	public AbstractFileHandlerDriver get(String handler)
	{
		return map.get(handler);
	}
	
	public boolean register(String handler, AbstractFileHandlerDriver driver)
	{
		if(map.containsKey(handler)) {
			return false;
		}
		
		map.put(handler, driver);
		return true;
	}
	
	public boolean remove(String handler)
	{
		if(!map.containsKey(handler)) {
			return false;
		}
		
		map.remove(handler);
		return true;
	}
	
	public String register(AbstractFileHandlerDriver driver)
	{
		String handle = driver.getHandle();
		if(this.register(handle, driver)) {
			return handle;
		}
		
		return null;
	}
}
